package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    public Quoter() {}

    public double getFahrenheit(String degrees) {
        double degreesInt = Double.parseDouble(degrees);

        return (degreesInt * 9 / 5) + 32;
    }

}
